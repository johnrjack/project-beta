from django.db import models

# Create your models here.


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True, null=True)
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17, unique=True)

    def __str__(self):
        return self.vin

class Technician(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class ServiceAppointment(models.Model):
    vin = models.CharField(max_length=18)
    customer = models.CharField(max_length=100)
    date = models.DateTimeField()
    reason = models.CharField(max_length=300)
    vip = models.BooleanField(default=False)
    completed = models.BooleanField(default=False)
    cancelled = models.BooleanField(default=False)
    technician = models.ForeignKey(
        Technician,
        related_name="techician",
        on_delete=models.PROTECT,
    )
    def __str__(self):
        return self.customer
